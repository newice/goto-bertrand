import { Injectable } from "@angular/core";
import { Network, DataSet, Edge, Node } from "vis";
import { Universe } from "./universe.service";

@Injectable({
  providedIn: "root"
})
export class DijkstraService {
  constructor() {}

  findShortestWay(
    start: Node,
    dest: Node,
    nodes: Node[],
    edges: Edge[]
  ): { nodes: number[]; edges?: any[]; costs: Number[] } {
    var Q = this.initialize(start, nodes);
    while (Q.filter(q => !q.visited).length > 0) {
     // debugger;
      var notVisitedNodes = Q.filter(q => !q.visited).sort(
        (a, b) => a.distance - b.distance
      );
      var nearest = notVisitedNodes[0];
      nearest.visited = true;
      if (nearest.label == dest.label) {
        return {
          nodes: nearest.pathNodeIds.concat([nearest.id]),
          edges: nearest.pathEdgeIds,
          costs: nearest.pathCosts.concat([nearest.distance])
        };
      }

      var neighbouredges = edges.filter(
        e => e.from == nearest.id || e.to == nearest.id
      );

      neighbouredges.forEach(e => {
        var id = e.from == nearest.id ? e.to : e.from;
        var dist = nearest.distance + e.value;
        var neighbour = Q.filter(n => n.id == id)[0];
        if (!neighbour.visited && neighbour.distance > dist) {
          neighbour.distance = dist;
          neighbour.pathNodeIds = nearest.pathNodeIds.concat([nearest.id]);
          neighbour.pathEdgeIds = nearest.pathEdgeIds.concat([e.id]);
          neighbour.pathCosts = nearest.pathCosts.concat([dist]);
        }
      });
    }

    return { nodes: [], edges: [], costs: [] };
  }

  private initialize(start: Node, nodes: Node[]) {
    return nodes.map((node, index) => {
      return {
        label: node.label,
        distance: node.label == start.label ? 0 : Number.MAX_SAFE_INTEGER,
        visited: false,
        pathNodeIds: [],
        pathEdgeIds: [],
        pathCosts: node.label == start.label ?[0]: [],
        id: index
      };
    });
  }
}
