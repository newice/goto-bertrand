import { Component, ViewChild, ElementRef } from "@angular/core";
import { Network, DataSet, Edge, Node, IdType } from "vis";
import { UniverseService, Universe } from "../universe.service";
import { options } from "../network-options";
import { forkJoin } from "rxjs";
import { DijkstraService } from "../dijkstra.service";

@Component({
  selector: "app-galaxy",
  templateUrl: "./galaxy.component.html",
  styleUrls: ["./galaxy.component.css"]
})
export class GalaxyComponent {
  start = "Erde";
  destination = "b3-r7-r4nd7";
  status = "Starting...";

  nodes: DataSet<Node> = new DataSet();
  edges: DataSet<Edge> = new DataSet();

  network: Network;
  universe: Universe;
  positions;
  directions: Node[] = null;
  currentIndex: number;
  directionCosts:any[];

  @ViewChild("network")
  public networknode: ElementRef;

  constructor(
    private universeService: UniverseService,
    private galacticAlgorithm: DijkstraService
  ) {
    this.status = "Loading universe...";
    forkJoin(
      this.universeService.getNodesAndEdges(),
      this.universeService.getNodePositions()
    ).subscribe(
      (data: [Universe, {}]) => {
        console.log(data);
        this.universe = data[0];
        this.positions = data[1];
        this.initNetwork();
      }
    );
  }

  getRandomAttributes(item) {
    if (item && item.label == this.start) return { group: "earth", value: 0.5 };
    if (item && item.label == this.destination)
      return { group: "bertrand", value: 0.999 };
    var group = Math.floor(Math.random() * 3) + 1;
    var value = Math.random() * 0.5;
    return { group: "g" + group, value: value };
  }

  navigate() {
    var start = this.nodes.get().filter(node => node.label == this.start);
    var dest = this.nodes.get().filter(node => node.label == this.destination);
    if (start.length == 1 && dest.length == 1) {
      var startNode = start[0];
      var destNode = dest[0];
      if (startNode.id != destNode.id) {
        this.network.moveTo({
          scale: 0.03,
          animation: true,
          position: {x:0,y:0}
        });
        var result = this.galacticAlgorithm.findShortestWay(
          startNode,
          destNode,
          this.nodes.get(),
          this.edges.get()
        );
        this.network.selectNodes(result.nodes, false);
        this.network.selectEdges(result.edges);
        this.directions = result.nodes.map(
          node => this.nodes.get().filter(n => n.id == node)[0]
        );
        this.directionCosts = result.costs;
        this.currentIndex = -1;
      }
    }
  }

  nextDirection(index:number) {
    this.currentIndex = index;
    var id = this.directions[this.currentIndex].id;

    var pos = this.network.getPositions(id);
    this.network.moveTo({
      scale: 1,
      animation: true,
      position: pos[id]
    });
  }

  initNetwork() {
    this.status = "Initializing network graph...";
    this.nodes.clear();
    this.edges.clear();

    var nodesData = this.universe.nodes.map((item, index) => {
      return Object.assign(
        {},
        { id: index },
        this.getRandomAttributes(item),
        item,
        this.positions[index]
      );
    });
    var edgesData = this.universe.edges.map(item => {
      return { from: item.source, to: item.target, value: item.cost };
    });
    this.nodes.add(nodesData);
    this.edges.add(edgesData);

    // provide the data in the vis format
    var data = {
      nodes: this.nodes,
      edges: this.edges
    };

    var physics = {
      physics: {
        enabled: false
      }
    };
    var opts = Object.assign({}, options, physics);

    console.log("starting network");
    // initialize your network!
    this.status = "Rendering universe...";
    this.network = new Network(this.networknode.nativeElement, data, opts);
  }
}
