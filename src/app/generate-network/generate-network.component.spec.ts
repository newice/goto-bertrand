import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateNetworkComponent } from './generate-network.component';

describe('GenerateNetworkComponent', () => {
  let component: GenerateNetworkComponent;
  let fixture: ComponentFixture<GenerateNetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateNetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
