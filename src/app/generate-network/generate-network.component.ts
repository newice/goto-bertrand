import { Component, ViewChild, ElementRef } from "@angular/core";
import { Network, DataSet, Edge, Node, Options } from "vis";
import { UniverseService, Universe } from "../universe.service";
import * as FileSaver from "file-saver";
import { options } from "../network-options";
import { Observable, forkJoin } from "rxjs";

@Component({
  selector: "app-generate-network",
  templateUrl: "./generate-network.component.html",
  styleUrls: ["./generate-network.component.css"]
})
export class GenerateNetworkComponent {
  start = "Erde";
  destination = "b3-r7-r4nd7";
  status = "Starting...";

  nodes: DataSet<Node> = new DataSet();
  edges: DataSet<Edge> = new DataSet();

  network: Network;
  universe: Universe;

  @ViewChild("network")
  public networknode: ElementRef;

  constructor(private universeService: UniverseService) {
    this.status = "Loading universe...";
    this.universeService.getNodesAndEdges().subscribe(
      (data: Universe) => {
        console.log(data);
        this.universe = data;
        this.initNetwork();
      },
      error => {}
    );
  }

  getRandomAttributes(item) {
    if (item && item.label == this.start) return { group: "earth", value: 0.5 };
    if (item && item.label == this.destination)
      return { group: "bertrand", value: 0.999 };
    var group = Math.floor(Math.random() * 3) + 1;
    var value = Math.random() * 0.5;
    return { group: "g" + group, value: value };
  }
  navigate() {}

  initNetwork() {
    this.status = "Initializing network graph...";
    console.log(this.status);
    this.nodes.clear();
    this.edges.clear();

    var nodesData = this.universe.nodes.map((item, index) => {
      return Object.assign(
        {},
        { id: index },
        this.getRandomAttributes(item),
        item
      );
    });
    var edgesData = this.universe.edges.map(item => {
      return { from: item.source, to: item.target, value: 1 - item.cost };
    });
    this.nodes.add(nodesData);
    this.edges.add(edgesData);

    // provide the data in the vis format
    var data = {
      nodes: this.nodes,
      edges: this.edges
    };
    var physics = {
      physics: {
        barnesHut: {
          gravitationalConstant: -8000,
          springConstant: 0.001,
          springLength: 300
        },
        stabilization: {
          enabled: true,
          iterations: 500,
          updateInterval: 100
        }
      }
    };
    var opts = Object.assign({}, options, physics);
    // initialize your network!
    this.status = "Rendering universe...";
    this.network = new Network(this.networknode.nativeElement, data, opts);
    this.network.on("stabilized", this.stabilized);
    console.log(this.network.getSeed());
  }
  downloadUniverse() {
    var blob = new Blob([JSON.stringify(this.network.getPositions())], {
      type: "text/plain;charset=utf-8"
    });
    FileSaver.saveAs(blob, "Universe.positions.json");
  }

  stabilized() {
    this.status = "Universe is stable;";
  }
}
