import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { retry } from "rxjs/operators";
import { environment } from "../environments/environment";
import { Observable } from "rxjs";

export interface Universe {
  nodes: { label: string }[];
  edges: {
    source: number;
    target: number;
    cost: number;
  }[];
}

@Injectable({
  providedIn: "root"
})
export class UniverseService {
  constructor(private http: HttpClient) {}

  getNodePositions() {
    return this.http.get<Universe>(environment.universePositionsPath).pipe(retry(3));
  }

  getNodesAndEdges(): Observable<Universe> {
    return this.http.get<Universe>(environment.universePath).pipe(retry(3));
  }
}
