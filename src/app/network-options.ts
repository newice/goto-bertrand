import { Options } from "vis";

export const options: Options = {
  groups: {
    earth: {
      shape: "circularImage",
      image: "assets/earth.png"
    },
    bertrand: {
      shape: "circularImage",
      image: "assets/bertrand.png"
    },
    g1: {
      shape: "circularImage",
      image: "assets/planet1.jpg"
    },
    g2: {
      shape: "circularImage",
      image: "assets/planet2.jpg"
    },
    g3: {
      shape: "circularImage",
      image: "assets/planet3.jpg"
    }
  },
  autoResize: true,
  height: window.innerHeight + "px",
  layout: {
    improvedLayout: false,
    hierarchical: false
  },
  interaction: {
    dragNodes: false,
    selectConnectedEdges: false,
    multiselect: true
  },
  nodes: {
    borderWidthSelected:5,
    scaling: { min: 10, max: 100 },
    shape: "circle",
    shapeProperties: {
      interpolation: false
    },
    color : {
      border:"#ffffff33",
      highlight:{
        border:"#ffffff"
      }
    },
    font: {
      background: "#cccccc",
      color: "#000000",
      size: 12,
      face: "arial"
    }
  },
  edges: {
    selectionWidth:15,
    color: {
      color: "#ffffff33",
      highlight: "#ffffff"
    }

  }
};
