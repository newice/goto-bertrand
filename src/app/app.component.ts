import { Component, ViewChild, ElementRef } from "@angular/core";
import { Network, DataSet, Edge, Node } from "vis";
import { UniverseService, Universe } from "./universe.service";
import * as FileSaver from "file-saver";
import { options } from "./network-options";
import { forkJoin } from 'rxjs';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
 
}
