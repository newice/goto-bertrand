import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { GenerateNetworkComponent } from "./generate-network/generate-network.component";
import { environment } from "src/environments/environment.prod";
import { GalaxyComponent } from './galaxy/galaxy.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const appRoutes: Routes = [
  { path: "", component: GalaxyComponent },
  { path: "generate", component: GenerateNetworkComponent }
];

@NgModule({
  declarations: [AppComponent, GenerateNetworkComponent, GalaxyComponent],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: !environment.production, useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
